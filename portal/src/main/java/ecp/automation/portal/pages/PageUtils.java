package ecp.automation.portal.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ecp.automation.portal.utils.Target;
import io.appium.java_client.android.AndroidDriver;

public class PageUtils {
	
	private static HomePage homePage=null;
	private static WebDriver driver=null;
	private static WebDriverWait wait=null;
	
	public static void initializeBrowser(String browser) {
		
		if(browser.equalsIgnoreCase(Target.FIREFOX_BROWSER.toString())) {
			//System.setProperty("webdriver.chrome.driver", "chromedriver.exe");			
			driver=new FirefoxDriver();
			
		}else if (browser.equalsIgnoreCase(Target.CHROME_BROWSER.toString())){
			driver=new ChromeDriver();
		} 
		else if (browser.equalsIgnoreCase(Target.ANDROID_CHROME.toString())){
			DesiredCapabilities caps=new DesiredCapabilities();
			caps.setCapability("deviceName", "samsung");
			caps.setCapability("platformVersion", "6.0");
			caps.setCapability("app", "Browser");
			driver=new AndroidDriver(caps);
		} 	
		
			driver.manage().window().maximize();
			
			driver.get("http://ecp.ae/Arabic/Pages/default.aspx");
			wait = new WebDriverWait(driver, 60); 
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='LoginMenu']")));
		    homePage= PageFactory.initElements(driver, HomePage.class);
		    
		    
		
	}
		
		public static HomePage getHomePage() {
			return homePage;
		}
		
		public static WebDriver getWebDriver() {
			return driver;
		}
		public static WebDriverWait getWebDriverWait() {
			return wait ;
		}

}
