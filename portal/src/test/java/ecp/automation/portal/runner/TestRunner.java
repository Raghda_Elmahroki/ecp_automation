package ecp.automation.portal.runner;


 
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
 
//format= {"html:target/surefire-reports/cuc.html"}
@CucumberOptions(features = "src/test/resources/features/Login.feature",plugin = {"pretty","html:target/html", "json:target/cucumber.json"},
        glue = "ecp.automation.portal.steps")
public class TestRunner extends AbstractTestNGCucumberTests {
}