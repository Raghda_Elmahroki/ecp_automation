package ecp.automation.portal.steps;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import ecp.automation.portal.pages.LoggedInPage;
import ecp.automation.portal.pages.LoginPage;
import ecp.automation.portal.pages.PageUtils;
import ecp.automation.portal.utils.Target;


public class LoginSteps {
   LoginPage loginPage=null;
   LoggedInPage loggedInPage=null;
	
	public LoginSteps() {
		
	}
	@Given("^I navigate to login page in browser\"([^\"]*)\"$")
	public void i_navigate_to_login_page_in_browser(String arg1)  {
	    // Write code here that turns the phrase above into concrete actions
		PageUtils.initializeBrowser(arg1);
	 loginPage= PageUtils.getHomePage().goToLoginPage(PageUtils.getWebDriver());
	}

	@Given("^I enter correct \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_correct_and(String arg1, String arg2){
	    // Write code here that turns the phrase above into concrete actions
		loginPage.fillUserName(arg1);
		loginPage.fillPassword(arg2);
	  
	}

	@Given("^I click submit$")
	public void i_click_submit()  {
	    // Write code here that turns the phrase above into concrete actions
		loggedInPage=loginPage.clickSubmit();
	
			   
	}


	@Then("^I see \"([^\"]*)\"$")
	public void i_see(String arg1) {
	    // Write code here that turns the phrase above into concrete actions
	   Assert.assertEquals(arg1, loggedInPage.getDisplayedUserName());
	}
}
