Feature: Login Feature
This feature deals with login functionality of the application

Scenario Outline:Login with correct username and password
Given I navigate to login page in browser"<browser>"
And I enter correct "<username>" and "<password>"
And I click submit
Then I see "<message>"
Examples:
|browser        |username|password |message |
|ANDROID_CHROME |raghda  |Iacad@123|raghda  |
